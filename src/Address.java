/**
 * Berner Fachhochschule</p>
 * Modul 8051-HS2012</p>
 * 
 * The class Address describes a mailing address. </p>
 * The class provides methods for getting and setting components of the address. </p>
 * The input data are not validated. </p>
 * 
 * Verbesserungsvorschlaege:
 * - Input validation of PLZ, Ort, Characters.
 * 
 * 
 * @author Johannes Gnaegi, gnaegj1@bfh.ch
 * @version 25.09.2012
 * 
 */
public class Address {

	/*
	 * Instance Variable for the name of the address. 
	 */
	private String name;
	/*
	 * Instnace variable for the forename of the address. 
	 */
	private String forename;
	/*
	 * Instance variable for the street where the address lives. 
	 */
	private String street;
	/*
	 * Instance variable for the streetnumber of the street where the address lives. 
	 */
	private String streetNumber;
	/*
	 * Instance variable for the postal code of the City where the address lives. 
	 */
	private String postalCode;
	/*
	 * Instance variable for the City where the address lives. 
	 */
	private String city; 

	/**
	 * Instances a new Object of Type <code>Adress</code>. 
	 * @param none
	 */
	public Address() {
		

	}
	
	/**
	 * Instances a new Object of Type <code>Adress</code> with no input validation.</p>
	 * <b>Example:</b> <i>new Address("Muster", "Hans", "Beispielstrasse", "12", "8000", "Beispiel")</i></p>
	 * @param <b>aName:</b> a name for the address.
	 * @param <b>aForename:</b> a forename for the address.
	 * @param <b>aStreet:</b> street where address lives
	 * @param <b>aStreetNumer:</b> street number of the street
	 * @param <b>aPostalCode:</b> postal code for the city
	 * @param <b>aCity:</b> name of the city
	 */
	public Address(String aName, 
			String aForename, 
			String aStreet,
			String aStreetNumber,
			String aPostalCode,
			String aCity) {
		
		setName(aName);
		setForename(aForename);
		setStreet(aStreet);
		setStreetNumber(aStreetNumber);
		setPostalCode(aPostalCode);
		setCity(aCity); 
	}

	/**
	 * Returns the name of the address. 
	 * @return the name of the address. 
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name of the address. 
	 * @param name - the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Returns the forename of the address.
	 * @return the forename of the address
	 */
	public String getForename() {
		return forename;
	}

	/**
	 * Sets the forename of the address.
	 * @param forename - the forename to set
	 */
	public void setForename(String forename) {
		this.forename = forename;
	}

	/**
	 * Returns the street of the address.
	 * @return the street of the address.
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * Sets the street of the address. 
	 * @param street - the street to set
	 */
	public void setStreet(String street) {
		this.street = street;
	}

	/**
	 * Returns the street number of the address.
	 * @return the streetNumber of the address.
	 */
	public String getStreetNumber() {
		return streetNumber;
	}

	/**
	 * Sets the street number of the address. 
	 * @param streetNumber - the streetNumber to set
	 */
	public void setStreetNumber(String streetNumber) {
		this.streetNumber = streetNumber;
	}

	/**
	 * Returns the postal code of the address.
	 * @return the postalCode of the address
	 */
	public String getPostalCode() {
		return postalCode;
	}

	/**
	 * Sets the postal code of the address. 
	 * Its a Number with 4 digits. 
	 * @param postalCode - the postalCode to set
	 */
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	/**
	 * Returns the city of the address. 
	 * @return the city of the address. 
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Sets the city of the address
	 * @param city - the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}
	
	/**
	 * Formats and returns the whole adress.
	 * @return the formatted address
	 */
	public String getAdress()
	{
		String address = name + " " + forename + 
				"\n" + street + " " +streetNumber +
				"\n" + postalCode + " " + city; 
		return address; 
	}
	
	/**
	 * Compares two Addresses for equality. Each item in the Address is compared
	 * with its similar item in the given Address. 
	 * 
	 * @param address - an address to compare with
	 * @return true if given address is similar to this address. 
	 */
	public boolean equals(Address address)
	{
		if (
				this.name.equalsIgnoreCase(address.getName())
				&& this.forename.equalsIgnoreCase(address.getForename())
				&& this.street.equalsIgnoreCase(address.getStreet())
				&& this.streetNumber.equalsIgnoreCase(address.getStreetNumber())
				&& this.city.equalsIgnoreCase(address.getCity())
				&& this.postalCode.equalsIgnoreCase(address.getPostalCode()));
		
		return true; 
	}

}
