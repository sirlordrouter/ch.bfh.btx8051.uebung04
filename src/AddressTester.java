
/**
 * Berner Fachhochschule</p>
 * Medizininformatik BSc</p>
 * Modul 8051-HS2012</p>
 * 
 * @author Johannes Gnaegi, gnaegj1@bfh.ch
 * @version 25.09.2012
 */
public class AddressTester {

	/**
	 * @param none
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Address address1 = 
				new Address("Muster", "Peter", "Bernstr.", "15", "3002", "Bern"); 
		
		Address address2 = 
				new Address("Mueller", "Petra", "Bielstrasse", "34", "2500", "Biel");
		
		Address address3 = 
				new Address("Muster", "Peter", "Bernstr.", "15", "3002", "Bern"); 
		
		/*
		 * Tests the creation of a new address instance. 
		 */
		System.out.println(address1.equals(address3));
		
		System.out.println("Ausgabe der Adresse 1"); 
		System.out.println(address1.getAdress()); 
		System.out.println("Erwartet: "); 
		System.out.println("Muster Peter\nBernstr. 15\n3002 Bern"); 
		
		System.out.println("\n\rAusgabe der Adresse 2"); 
		System.out.println(address2.getAdress()); 
		System.out.println("Erwartet: "); 
		System.out.println("Mueller Petra\nBielstrasse 34\n2500 Biel"); 
		
		address1.setName("Meier");
		address1.setForename("Urs");
		address1.setStreet("Gasse");
		address1.setStreetNumber("200");
		address1.setPostalCode("8000");
		address1.setCity("Zuerich"); 
		
		System.out.println("\n\rAusgabe der Adresse 1"); 
		System.out.println(address1.getAdress()); 
		System.out.println("Erwartet: "); 
		System.out.println("Meier Urs\nGasse 200\n8000 Zuerich\n\r"); 
		
		System.out.println(address1.getName()); 
		System.out.println("Erwartet: Meier\n\r"); 
		
		System.out.println(address1.getForename()); 
		System.out.println("Erwartet: Urs\n\r"); 
		
		System.out.println(address1.getStreet()); 
		System.out.println("Erwartet: Gasse\n\r"); 
		
		System.out.println(address1.getStreetNumber()); 
		System.out.println("Erwartet: 200\n\r"); 
		
		System.out.println(address1.getCity()); 
		System.out.println("Erwartet: Zuerich\n\r"); 
		
		address2.setForename("Irene");
		address2.setStreet("Solothurnstrasse");
		address2.setStreetNumber("333"); 
		
		System.out.println("\n\rAusgabe der Adresse 2"); 
		System.out.println(address2.getAdress()); 
		System.out.println("Erwartet: "); 
		System.out.println("Mueller Irene\nSolothurnstrasse 333\n2500 Biel"); 
		
	}

}
